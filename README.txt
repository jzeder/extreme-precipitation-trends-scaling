READ ME - Master’s Thesis Joel Zeder
====================================

1) Input data:
- Original precipitation data in 'Data/<AHYD,DWD,ECA,EOBS,MCH>'
- Original temperature data in 'Data/Temperature/'
- Rx1d to Rx31d or #R95p and #R99p indices, surface temperature data,
and count data for histograms/empirical densities in 'Data/Indices/'
- Underlying data for maps in 'Data/Maps/'

2) Plots output:
All plots in 'Output/Plots/<Script_Chapter>', where the subfolder
structure represents partly the structure of the scripts and of the 
chapters of the thesis:
	1_Algorithm_Check 	 -> Compares indices derived from daily ECA&D data
						 	with Rx1day and Rx5day indices from ECA&D
	2_Metadata		 -> Different plots not directly related to scaling
						 	or trend estimates
	3_Nonpar_Trends	  	 -> All plots related to temporal trends
	4_EVD_Trends	  	 -> All plots related to univariate scaling estimates
							and field significance
	5_Spatial_Clustering 	 -> All plots related to clustering and weighted
							averaging
	6_Spatial_Process	 -> All plots related to the spatial variograms of
							and the return value estimation as a function
							of altitude

3) Data output:
All datasets in 'Output/Data/':
	df_res_temp    -> Temporary dataframes of results, only the last 19
				      are kept (the older ones are deleted regularly)
	df_res_master  -> Dataframe with all univariate results (which is
				 	  updated with values in df_res_temp when one types
				 	  'join_dataframes(results=T)')
	_resample      -> All resampling results are kept (not always the 
				 	  resampled time series, as it uses to much space)
	_MaxStabModels -> Max-stable process model fits are stored here
					  (these are all primarily results used for the
					  testing of these models.
					  
4) How-to:
- Presets in '0_Basic_Settings.R':
	In this script, many things are preset which are relevant for all
	scripts (otherwise, the presets are done in the header of the specific
	scripts)
- Run scripts:
	Always run header first, so that necessary paths are set and datasets
	are loaded.
- Apply "save_res" option:
	If set to true, results are saved in df_res_temp. These are concatenated
	to df_res_master if command is typed: 'join_dataframes(results=T)'
- Produce extreme precipitation and temperature index dataframes:
	Run the scripts in '1_Data_Reader.R' and then run
	'join_dataframes(precip=T)' or 'join_dataframes(temp=T)', respectively.
- Metadata on stations:
	Metadata on stations is saved in 'df_meta_d_used'
- Indices of subregions in df_xxxx_d dataframes:
	Indices of such stations are stored in list 'ind$<subregion>', e.g.
	'ind$AS' for southern Alpine region AS.
- Region/Altitude/Country-statistics on scaling (e.g. averages,
	weighted averages, return periods, average cluster size etc.) are
	found in 'df_trendstat_d_test_weight', can be loaded via
	'load(paste0("../Output/Data/df_res_test_weight",temp_name,".RData"))'
- Resampling on IAC servers:
	See manual in scripts '3_Nonpar_Regression.R' and '4_EVD_Regression.R'
- 100'000 resamplings of non-stationary GEV with \alpha_CC=0.065:
	These results (estimates of these 100'000 resamplings can be loaded
	with 'load(paste0("../Output/Data/df_bootstrap_gev.Rdata"))'









