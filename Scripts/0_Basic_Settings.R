## ----------------------------------------------------------------------------------------------
## Load libraries
## ==============

# Data handling libraries:
library(readr)
library(dplyr)
library(zoo)
library(stargazer)
library(reshape2)

# Plotting libraries:
library(ggplot2)
library(ggmap)
library(ggpubr)
#library(georob)
library(geosphere)
library(RColorBrewer)
library(rworldmap)
library(animation)
library(scatterpie)
library(scales)
library(corrplot)

# Spatial data packages:
library(grid)
#library(rgdal) -> Cannot be loaded
library(sp)
library(rgeos)
library(raster)

# Test libraries:
library(mblm)
library(trend.acwd)
library(boot)
library(parallel)
library(evd)
library(fitdistrplus)
library(SpatialExtremes)
library(extRemes)

# Used by Ribes (2018)
library(maps)
library(gstat) 
library(fields)
library(MASS)
library(nlme)
library(colorspace)
library(ismev)
library(cluster)

## ----------------------------------------------------------------------------------------------
## SETTINGS
## ========

# Time period of interest (in numeric form for zoo-index):
assign(x="time_per",value=1899:2016,envir=.GlobalEnv)

# Criteria for min duration in time period of interest:
assign(x="min_dur",value=80,envir=.GlobalEnv)

# Ref period for 95 and 99% quantiles:
assign(x="ref_per_quant",value=1961:1990,envir=.GlobalEnv)

# Minimum amount of days within year/halfyear/season:
min_fraction <- 0.9
assign(x="mincount_year",value=min_fraction*365,envir=.GlobalEnv)
assign(x="mincount_hyea",value=min_fraction*182,envir=.GlobalEnv)
assign(x="mincount_seas",value=min_fraction*90,envir=.GlobalEnv)

# Mean lat/lon-coord for maps:
mean_coord <- c(lon=9.62159,lat=49.74753) #c(lon=mean(df_meta_d_used$Lon),lat=mean(df_meta_d_used$Lat))

# Assign Google API
ggmap::register_google(key="AIzaSyDI6NFWK2hLRY1kXDUWK1oKk1SSpQ7Pl-M")

# Plot size:
assign(x="plotwd",value=5,envir=.GlobalEnv)
assign(x="plotht",value=3,envir=.GlobalEnv)

# Map for plots:
#style1 <- c(feature = "all", element = "labels", visibility = "off")
#style2 <- c("&style=", feature = "road", element = "geometry", visibility = "off")
#style  <- c(style1, style2)
#map <- get_googlemap(center = mean_coord, zoom = 5, maptype = "terrain", color = "bw",style = style)
# DO NOT USE: ---------------------------------------------------------------------------
#try_map <- try(map <- get_googlemap(center = mean_coord, zoom = 5, maptype = "terrain",
#                                         color = "bw",style = 'feature:all|element:labels|visibility:off'),silent=T)
#if(!"try-error" %in% class(try_map)) assign(x="map",value=map,envir=.GlobalEnv)
## --------------------------------------------------------------------------------------

#map_world <- map_data("world2")
#save(map,map_world,list=c("map","map_world"),file="../Data/Maps/maps_new.RData")
load("../Data/Maps/maps_new.RData")


# Path to index-dataframes and result dataframes:
assign(x="path_data",value='../Data/Indices/',envir=.GlobalEnv)
assign(x="path_results",value='../Output/Data/',envir=.GlobalEnv)

# Path to backup of scripts on Polybox:
assign(x="path_backup",value="/Users/Studium/polybox/Masterarbeit/Backup/Scripts",envir=.GlobalEnv)

# Colors for trend significance (neg non-sign, neg sig, pos non-sign, pos sign):
trend_colours_map  <- c("brown","orange",rgb(110,167,224,maxColorValue=250),"darkblue")
trend_colours_hist <- c("orange","brown",rgb(110,167,224,maxColorValue=250),"darkblue")

# Colors for countries (order: NL, DE, AT, CH)
assign(x="country_colours",value=c(brewer.pal(8,"Accent")[c(5,8,7)],brewer.pal(8,"Set1")[1]),envir=.GlobalEnv)

# Colors for regions (order: AS, ANW, ANE, PAW, PAE, LW, LE)
assign(x="regions_colours",value=c(brewer.pal(9,"Set1")[3:9]),envir=.GlobalEnv)
regions_colours_white <- regions_colours; regions_colours_white[4] <- "darkgoldenrod1"
assign(x="regions_colours_white",value=regions_colours_white,envir=.GlobalEnv)

# Long qualitative colour bar:
assign(x="cluster_colours",value=c(brewer.pal(12,"Set3"),brewer.pal(8,"Set2")[c(4,6:8)]),envir=.GlobalEnv)

# Season colours: ("year"   "Winter" "Summer" "DJF"    "MAM"    "JJA"    "SON")
assign(x="seasons_colours",value=c("grey","#8da0cb","#fc8d62","#8da0cb","#66c2a5","#fc8d62","#e78ac3"),
       envir=.GlobalEnv)

# Standard shape parameter prescribed for GEV-estimation:
assign(x="pres_shape_param",value=0.1008,envir=.GlobalEnv)
# or alternatively (result of (1-mean(mas$Weib_shape))/(mean(mas$Weib_shape)*log(150)))
#assign(x="pres_shape_param",value=0.007740331,envir=.GlobalEnv)

## ----------------------------------------------------------------------------------------------
## FUNCTIONS
## =========

# Create directories in certain 'path' and with certain 'names':
create_dir <- function(path,dir_names,subdir_names=NA) {
  for(i in 1:length(dir_names)) {
    dir.create(file.path(path, dir_names[i]), showWarnings = FALSE)
    if(!all(is.na(subdir_names))) {
      for(j in 1:length(subdir_names))
      dir.create(file.path(paste(path,dir_names[i],sep="/"), subdir_names[j]), showWarnings = FALSE)}}
}

# Perform backup to Polybox
backup_scripts <- function() {
  scripts_path <- getwd()
  dir_title <- paste0("Backup_",substr(Sys.time(),1,10),"_",substr(Sys.time(),12,13),substr(Sys.time(),15,16))
  create_dir(path_backup,dir_title)
  #file.copy(from=scripts_path,to=paste(path_backup,dir_title,sep="/"),recursive=T)
  files <- list.files(scripts_path)
  for(file in files) file.copy(from=paste(scripts_path,file,sep="/"),
                               to=paste(path_backup,dir_title,file,sep="/"),
                               copy.date=T)
}
backup_scripts()

# Load used stations only and assign GAR HISTALP region (Auer et al. 2007):
used_stations <- function() {
  df_meta_d_used <- df_meta_d[which(df_meta_d$Used==T),]
  #df_meta_d_used <- df_meta_d_used[-grep("DWD03988",df_meta_d_used$ID),]
  
  # Add information on HISTALP classification:
  load("../Data/Maps/shape_histalp.Rdata")
  sp_meda_d_used <- df_meta_d_used
  coordinates(sp_meda_d_used) <- ~ Lon + Lat
  proj4string(sp_meda_d_used) <- proj4string(shape_histalp)
  df_meta_d_used[,"GAR_reg"] <- sp::over(sp_meda_d_used,shape_histalp)
  df_meta_d_used$GAR_reg[is.na(df_meta_d_used$GAR_reg)] <- 4
  df_meta_d_used$GAR_reg <- factor(df_meta_d_used$GAR_reg,
                                   labels=c("SW","NW","NE","SE","none"))
  df_meta_d_used[,"Reg"] <- factor(NA,levels=c("AS","ANW","ANE","MW","ME","CW","CE"),ordered=T)
  df_meta_d_used <- regional_assignment(df_meta_d_used)
  return(df_meta_d_used)
}

# Regional assignment of all stations:
regional_assignment <- function(df_meta_d_used) {
  # Alpine south:
  ind_temp <- which(df_meta_d_used$GAR_reg=="SW" | df_meta_d_used$GAR_reg=="SE")
  df_meta_d_used$Reg[ind_temp] <- as.factor("AS")
  # Alpine northwest:
  ind_temp <- which((df_meta_d_used$GAR_reg=="NW" | df_meta_d_used$Lon<=7.5) & df_meta_d_used$Lat<47.9)
  df_meta_d_used$Reg[ind_temp] <- as.factor("ANW")
  # Alpine northeast:
  ind_temp <- which((df_meta_d_used$GAR_reg=="NE" & df_meta_d_used$Lat<47.9) |
                    (df_meta_d_used$Origin=="AHY" & df_meta_d_used$Lat>=47.9)) #(!df_meta_d_used$GAR_reg=="NW") & (!df_meta_d_used$Reg=="AS")))
  df_meta_d_used$Reg[ind_temp] <- as.factor("ANE")
  # Pre-alpine west:
  ind_temp <- which((df_meta_d_used$Origin=="DWD" & df_meta_d_used$Lat>=47.9) &
                      (df_meta_d_used$Lat<(49.9+df_meta_d_used$Lon*1/6)) &
                      (df_meta_d_used$Lat<(54.5-(df_meta_d_used$Lon-9.9)*6.5/1.5)))
  df_meta_d_used$Reg[ind_temp] <- as.factor("MW")
  # Pre-alpine east:
  ind_temp <- which((df_meta_d_used$Origin=="DWD" & df_meta_d_used$Lat>=47.9) &
                      (df_meta_d_used$Lat<(49.9+df_meta_d_used$Lon*1/6)) &
                      (df_meta_d_used$Lat>(54.5-(df_meta_d_used$Lon-9.9)*6.5/1.5)))
  df_meta_d_used$Reg[ind_temp] <- as.factor("ME")
  # Low west:
  ind_temp <- which((df_meta_d_used$Lat>(49.9+df_meta_d_used$Lon*1/6)) &
                    (df_meta_d_used$Lat<(54.5-(df_meta_d_used$Lon-9.9)*6.5/1.5)))
  df_meta_d_used$Reg[ind_temp] <- as.factor("CW")
  # Low east:
  ind_temp <- which((df_meta_d_used$Lat>(49.9+df_meta_d_used$Lon*1/6)) &
                      (df_meta_d_used$Lat>(54.5-(df_meta_d_used$Lon-9.9)*6.5/1.5)))
  df_meta_d_used$Reg[ind_temp] <- as.factor("CE")
  df_meta_d_used$Reg[which(is.na(df_meta_d_used$Reg))] <- as.factor("CW")
  return(df_meta_d_used)
}

# Create dataframe for results or if available add results to it:
save_results <- function(df) {
  df$ID <- as.character(df$ID)
  df_res_temp_comcol <- df_res_temp[,c(1,which(!names(df_res_temp) %in% names(df)))]
  df_res_temp_comcol$ID <- as.character(df_res_temp_comcol$ID)

  if(dim(df)[1]!=dim(df_res_temp_comcol)[1]) warning("Dimensions don't match!")
  if(!all(df$ID==df_res_temp_comcol$ID)) warning("IDs don't match!")
  
  df_res_temp <<- merge(df_res_temp_comcol,df,by="ID",all=T)
  #for(col_name in names(df)) df_res_temp[,col_name] <- df[,col_name]
  #assign(x="df_res_temp",value=df_res_temp,envir=.GlobalEnv)
}

load_create_res_temp <- function(no_new_df=F,add_naming="") {
  list_files_output <- list.files(path_results)
  # Create new file df_res_temp (if wanted)
  if(!no_new_df) new_df <- readline("Do you want to create a new 'df_res_temp' dataframe? (If so answer 'yes') ") else new_df <- "no"
  if(length(list_files_output)==0 | new_df=="yes") {
    time_str <- format(Sys.time(), "_%Y-%m-%d_%H%M")
    df_res_temp <- data.frame(ID=df_meta_d_used$ID,Alt=df_meta_d_used$Alt,
                              Lat=df_meta_d_used$Lat,Lon=df_meta_d_used$Lon)
    save(df_res_temp,file=paste(path_results,"df_res_temp",add_naming,time_str,".RData",sep=""))
  }
  
  # Load latest file df_res_temp
  last_df_temp <- list_files_output[length(list_files_output)]
  load(paste(path_results,last_df_temp,sep=""))
  
  # Delete first temporary file df_res_temp if more than 20 are around
  if(length(list_files_output[grep("df_res_temp",list_files_output)]) >= 20)
    file.remove(paste0(path_results,list_files_output[grep("df_res_temp",list_files_output)][1]))
  
  return(df_res_temp)
}

# Adjustable precipitation colorbar:
precip_col <- function(n) {
  if(n<10) {return(brewer.pal(n,"YlGnBu"))} else
    if(n>=10 & n<13) {
      r <- c(255,227,191,134,70,47,55,42,32,38,37,87)
      g <- c(255,253,255,242,179,152,132,90,59,0,0,28)
      b <- c(255,181,194,162,141,133,165,160,122,132,54,72)
      p_col_many <- rgb(r,g,b,maxColorValue = 255)
      return(colorRampPalette(p_col_many)(n))
    } else {warning("More than 12 colors not allowed!")}
}

# Simon Scherrer's Gauss-Smoother:
gauss.smooth <- function (x, window, lowp = TRUE) {
  if (window < 1) {
    stop("window < 1 unsinnig!")
  }
  if (window == 1) {
    wght <- 1
  }
  else {
    wght <- rep(NA, 2 * window)
    z <- 6/window
    ii <- 1
    repeat {
      wght[ii] <- dnorm((ii - 1) * z)
      if ((ii > 1) & (wght[ii] < (wght[1]/10))) {
        break
      }
      ii <- ii + 1
    }
    wght <- c(rev(wght[!is.na(wght)][-1]), wght[!is.na(wght)])
    wght <- wght/sum(wght)
  }
  smoothed.x <- stats::filter(x, wght, method = "convolution", sides = 2,
                       circular = FALSE)
  if (lowp == TRUE) {
    return(smoothed.x)
  }
  else {
    return(x - smoothed.x)
  }
}

# Expand on 7 times longer index vector (help function for "ind_search()":
expand7 <- function(ind) {
  ind7 <- rep((ind-1)*7+1,each=7) + c(0:6)
  return(ind7)
}

reduce7 <- function(ind) {
  ind_short <- (ind-1)/7
  ind_short <- ind_short[seq(1,length(ind),7)]+1
  return(ind_short)
}

# Calculates the indices of different data-origins (e.g. DWD, MCH..) and seasons (year, DJF..) of e.g. df_Rx1d_d
ind_search <- function(df) {
  if(T != all.equal(c(substr(names(df)[grep(".year",names(df))],1,
                             nchar(names(df)[grep(".year",names(df))])-5)),df_meta_d_used$ID)) {
    warning("(Order of) Names in index-dataframe is not the same as df_meta_d_used$ID:\nOnly IDs of origins and seasons returned")
    ind <- list(DWD=grep("DWD",names(df)),
                MCH=grep("MCH",names(df)),
                ECA=grep("ECA",names(df)),
                AHY=grep("AHY",names(df)),
                year=grep(".year",names(df)),
                Winter=grep(".Winter",names(df)),
                Summer=grep(".Summer",names(df)),
                DJF=grep(".DJF",names(df)),
                MAM=grep(".MAM",names(df)),
                JJA=grep(".JJA",names(df)),
                SON=grep(".SON",names(df))
    )
    return(ind)
  } else {
    ind <- list(all=grep(".",names(df)),
                DWD=grep("DWD",names(df)),
                MCH=grep("MCH",names(df)),
                ECA=grep("ECA",names(df)),
                AHY=grep("AHY",names(df)),
                year=grep(".year",names(df)),
                Winter=grep(".Winter",names(df)),
                Summer=grep(".Summer",names(df)),
                DJF=grep(".DJF",names(df)),
                MAM=grep(".MAM",names(df)),
                JJA=grep(".JJA",names(df)),
                SON=grep(".SON",names(df)),
                alt02=expand7(which(df_meta_d_used$Alt<=200)),
                alt24=expand7(which(df_meta_d_used$Alt<=400 &df_meta_d_used$Alt>200)),
                alt46=expand7(which(df_meta_d_used$Alt<=600 &df_meta_d_used$Alt>400)),
                alt68=expand7(which(df_meta_d_used$Alt<=800 &df_meta_d_used$Alt>600)),
                alt810=expand7(which(df_meta_d_used$Alt<=1000 &df_meta_d_used$Alt>800)),
                alt1015=expand7(which(df_meta_d_used$Alt<=1500 &df_meta_d_used$Alt>1000)),
                alt1520=expand7(which(df_meta_d_used$Alt<=2000 &df_meta_d_used$Alt>1500)),
                alt2030=expand7(which(df_meta_d_used$Alt<=3000 &df_meta_d_used$Alt>2000)),
                end2000=expand7(which(df_meta_d_used$e.Date>=as.Date("2000-01-01"))),
                AS=expand7(which(df_meta_d_used$Reg=="AS")),
                ANW=expand7(which(df_meta_d_used$Reg=="ANW")),
                ANE=expand7(which(df_meta_d_used$Reg=="ANE")),
                MW=expand7(which(df_meta_d_used$Reg=="MW")),
                ME=expand7(which(df_meta_d_used$Reg=="ME")),
                CW=expand7(which(df_meta_d_used$Reg=="CW")),
                CE=expand7(which(df_meta_d_used$Reg=="CE")))
    return(ind)
  }
}

# Calculates common indices of above function returns
com_ind <- function(ind1,ind2) {
  list_of_num_vec <- list(ind1,ind2)
  intersect <- Reduce(intersect, list_of_num_vec)
  return(intersect)
}

# Join dataframes into df_d, df_temp, and df_results:
join_dataframes <- function(precip=F,temp=F,counts=F,results=F) {
  if(!any(c(precip,temp,results,counts))) print("No dataframes selected to be joined! -> Set one to TRUE")
  if(precip) {
    rm(list=ls()[grep("df_",ls())])
    load(paste(path_data,"df_d_MCH.RData",sep=""))
    df_meta_d_all <- df_meta_d
    df_mean_d_all <- df_mean_d
    df_R95p_d_all <- df_R95p_d
    df_R99p_d_all <- df_R99p_d
    df_Rx1d_d_all <- df_Rx1d_d
    df_Rx1d_d_date_all <- df_Rx1d_d_date
    df_Rx3d_d_all <- df_Rx3d_d
    df_Rx3d_d_date_all <- df_Rx3d_d_date
    df_Rx5d_d_all <- df_Rx5d_d
    df_Rx5d_d_date_all <- df_Rx5d_d_date
    df_Rx7d_d_all <- df_Rx7d_d
    df_Rx7d_d_date_all <- df_Rx7d_d_date
    df_Rx31d_d_all <- df_Rx31d_d
    df_Rx31d_d_date_all <- df_Rx31d_d_date
    
    for(dataset in c("df_d_ECA.RData","df_d_DWD.RData","df_d_AHY.RData")) {
      load(paste(path_data,dataset,sep=""))
      df_meta_d_all <- bind_rows(df_meta_d,df_meta_d_all)
      df_mean_d_all <- cbind(df_mean_d,df_mean_d_all)
      df_R95p_d_all <- cbind(df_R95p_d,df_R95p_d_all)
      df_R99p_d_all <- cbind(df_R99p_d,df_R99p_d_all)
      df_Rx1d_d_all <- cbind(df_Rx1d_d,df_Rx1d_d_all)
      df_Rx1d_d_date_all <- cbind(df_Rx1d_d_date,df_Rx1d_d_date_all)
      df_Rx3d_d_all <- cbind(df_Rx3d_d,df_Rx3d_d_all)
      df_Rx3d_d_date_all <- cbind(df_Rx3d_d_date,df_Rx3d_d_date_all)
      df_Rx5d_d_all <- cbind(df_Rx5d_d,df_Rx5d_d_all)
      df_Rx5d_d_date_all <- cbind(df_Rx5d_d_date,df_Rx5d_d_date_all)
      df_Rx7d_d_all <- cbind(df_Rx7d_d,df_Rx7d_d_all)
      df_Rx7d_d_date_all <- cbind(df_Rx7d_d_date,df_Rx7d_d_date_all)
      df_Rx31d_d_all <- cbind(df_Rx31d_d,df_Rx31d_d_all)
      df_Rx31d_d_date_all <- cbind(df_Rx31d_d_date,df_Rx31d_d_date_all)
    }
    
    df_meta_d_all -> df_meta_d
    df_mean_d_all -> df_mean_d
    df_R95p_d_all -> df_R95p_d
    df_R99p_d_all -> df_R99p_d
    df_Rx1d_d_all -> df_Rx1d_d
    df_Rx1d_d_date_all -> df_Rx1d_d_date
    df_Rx3d_d_all -> df_Rx3d_d
    df_Rx3d_d_date_all -> df_Rx3d_d_date
    df_Rx5d_d_all -> df_Rx5d_d
    df_Rx5d_d_date_all -> df_Rx5d_d_date
    df_Rx7d_d_all -> df_Rx7d_d
    df_Rx7d_d_date_all -> df_Rx7d_d_date
    df_Rx31d_d_all -> df_Rx31d_d
    df_Rx31d_d_date_all -> df_Rx31d_d_date
    
    # Set all -Inf in df_Rx31d_d to NA
    df_Rx31d_d <- do.call(data.frame,lapply(df_Rx31d_d, function(x) replace(x, is.infinite(x),NA)))
    df_Rx31d_d <- zoo(df_Rx31d_d,order.by = index(df_Rx1d_d))
    
    # Set all values in #R95p and #R99p close to zero (no counts of precipitation higher
    # than 95%/99%-quantile) to NA:
    df_R95p_d <- do.call(data.frame,lapply(df_R95p_d, function(x) replace(x, 1>x,NA)))
    df_R99p_d <- do.call(data.frame,lapply(df_R99p_d, function(x) replace(x, 1>x,NA)))
    df_R95p_d <- zoo(df_R95p_d,order.by = index(df_Rx1d_d))
    df_R99p_d <- zoo(df_R99p_d,order.by = index(df_Rx1d_d))
    
    rm(list=ls()[grep("_all",ls())])
    
    # Select series with required data length based on Rx1d: (min_dur)
    ID_drop        <- names(df_Rx1d_d[,colSums(!is.na(df_Rx1d_d))<min_dur])
    ID_drop_unique <- unique(substr(ID_drop,1,9))
    ID_all         <- substr(names(df_Rx1d_d),1,9)
    df_Rx1d_d      <-  df_Rx1d_d[,!(ID_all %in% ID_drop_unique)]
    df_Rx3d_d      <-  df_Rx3d_d[,!(ID_all %in% ID_drop_unique)]
    df_Rx5d_d      <-  df_Rx5d_d[,!(ID_all %in% ID_drop_unique)]
    df_Rx7d_d      <-  df_Rx7d_d[,!(ID_all %in% ID_drop_unique)]
    df_Rx31d_d     <- df_Rx31d_d[,!(ID_all %in% ID_drop_unique)]
    df_mean_d      <-  df_mean_d[,!(ID_all %in% ID_drop_unique)]
    df_R95p_d      <-  df_R95p_d[,!(ID_all %in% ID_drop_unique)]
    df_R99p_d      <-  df_R99p_d[,!(ID_all %in% ID_drop_unique)]
    
    ID_used        <- unique(substr(names(df_Rx1d_d),1,9))
    ID_used[grepl("DWD",ID_used)] <- substr(ID_used[grepl("DWD",ID_used)],1,8)
    ID_used[grepl("MCH",ID_used)] <- substr(ID_used[grepl("MCH",ID_used)],1,8)
    df_meta_d$Used[!(df_meta_d$ID %in% ID_used)] <- F
    
    save(list=ls()[grep("df_",ls())],file=paste(path_data,"df_d.RData",sep=""))
  }
  
  if(temp) {
    rm(list=ls()[grep("df_",ls())])
    load(paste(path_data,"df_temp_MCH.RData",sep=""))
    index <- df_temp_d$time
    df_temp_d_all <- df_temp_d[,-1]
    
    load(paste(path_data,"df_temp_HCR55.RData",sep=""))
    df_temp_d_all <- cbind(df_temp_d[,-1],df_temp_d_all)
    
    load(paste(path_data,"df_temp_HCRNH.RData",sep=""))
    df_temp_d_all <- cbind(df_temp_d[,-1],df_temp_d_all)
    
    load(paste(path_data,"df_temp_HCRGL.RData",sep=""))
    df_temp_d_all <- cbind(df_temp_d[,-1],df_temp_d_all)
    
    load(paste(path_data,"df_temp_GISSNH.RData",sep=""))
    df_temp_d_all <- cbind(df_temp_d[,-1],df_temp_d_all)
    
    load(paste(path_data,"df_temp_GISSGL.RData",sep=""))
    df_temp_d_all <- cbind(df_temp_d[,-1],df_temp_d_all)
    
    df_temp_d_all -> df_temp_d
    rm(list=ls()[grep("_all",ls())])
    rownames(df_temp_d) <- index
    
    df_temp_d <- zoo(df_temp_d,order.by = index)
    save(list=ls()[grep("df_",ls())],file=paste(path_data,"df_temp.RData",sep=""))
  }
  
  if(results) {
    list_master_files <- list.files(path_results)[grep("df_res_master",list.files(path_results))]
    
    df_res_master <- load_master()
    load(paste(path_results,list_master_files[length(list_master_files)],sep=""))
    if(!all(df_res_master$ID==df_res_temp$ID)) stop("IDs don't match!")
    if(any(names(df_res_master) %in% names(df_res_temp))) {
      print("Common columns which would be replace:")
      print(names(df_res_master[names(df_res_master) %in% names(df_res_temp)]))
      if(!readline("Do you want to overwrite common columns in 'df_res_master' (Answer Yes or No): ")=="Yes") {
        stop("No join performed")} else {
          for(col_name in names(df_res_temp)[-c(1:4)]) df_res_master[,col_name] <- df_res_temp[,col_name]
        }
    } else {
      df_res_master <- cbind(df_res_master,df_res_temp)
    }
    time_str <- format(Sys.time(), "_%Y-%m-%d_%H%M")
    save(df_res_master,file=paste(path_results,"df_res_master",time_str,".RData",sep=""))
    print("'df_res_master' updated and saved")
    rm(df_res_master)
  }
  
  if(counts) {
    load(paste(path_data,"dm_count_d_MCH.RData",sep=""))
    dm_count_d_all <- dm_count_d
    for(dataset in c("dm_count_d_AHY.RData","dm_count_d_ECA.RData",
                     "dm_count_d_DWD.RData")) {
      load(paste(path_data,dataset,sep=""))
      dm_count_d_all <- dm_count_d_all + dm_count_d
    }
    dm_count_d_all -> dm_count_d
    rm(list=ls()[grep("_all",ls())])
    save(dm_count_d,file=paste(path_data,"dm_count_d.RData",sep=""))
  }
}

# Load (current) df_res_master file into environment:
load_master <- function(which_last_master_file=0) {
  # which_last_master_file -> If insert 1, get second-most-current df_res_master file, if insert 2, get third-most-current...
  list_master_files <- list.files(path_results)[grep("df_res_master",list.files(path_results))]
  load(paste(path_results,list_master_files[length(list_master_files)-which_last_master_file],sep=""))
  return(df_res_master)
  #assign(x="df_res_master",value=df_res_master,envir=.GlobalEnv)
}


get_country_spatialpoly <- function(spatialpolygon=T) {
  # Import DEM Data:
  worldMap <- getMap(resolution="high")
  
  # Member States of the European Union
  countries <- c("Austria","Germany","Netherlands","Switzerland","Liechtenstein")
  # Select only the index of states member of the E.U.
  indcountries <- which(worldMap$NAME%in%countries)
  coords <- lapply(indcountries, function(i){
    df <- data.frame(worldMap@polygons[[i]]@Polygons[[1]]@coords)
    df$region =as.character(worldMap$NAME[i])
    colnames(df) <- list("long", "lat", "region")
    return(df)
  })
  
  coords <- do.call("rbind", coords)
  if(!spatialpolygon) return(coords)
  coords_list <- split(coords, coords$region)
  coords_p <- lapply(coords_list, function(x) Polygon(x[,1:2]))
  coords_ps <- lapply(seq_along(coords_p), function(i) Polygons(list(coords_p[[i]]), 
                                                                ID = names(coords_list)[i]))
  coords_sps <- SpatialPolygons(coords_ps) #, proj4string = CRS("+proj=longlat +datum=WGS84") ) 
  return(coords_sps)
}

# Get altitude raster:
get_altitude_raster <- function(coords_sps) {
  DEM <- raster("../Data/Maps/DEM_geotiff/alwdgg.tif")
  DEM_count <- crop(DEM, extent(coords_sps))
  DEM_count <- mask(DEM_count, coords_sps)
  return(DEM_count)
}