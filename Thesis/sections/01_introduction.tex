	\chapter{Introduction}\label{sec:intro}
	
This introduction chapter is preparing the ground for the upcoming analysis of the temperature sensitivity and scaling of extreme precipitation in Central Europe. In a first section, the general motivation why to investigate this subject is outlined with a brief wrap-up on the current state of research. A more thorough overview is provided in Section~\ref{ssec:physbas_CCscalingl}, after the relevant physical mechanisms have been introduced. Furthermore, research objectives are formulated, and the structuring of the thesis is presented.
	
	\section{Motivation}\label{ssec:bg_mot}

Extreme precipitation events pose a major environmental risk in Central Europe, primarily in terms of wide-spread flooding \citep[e.g.][]{Wake2013}, triggering of land slides and debris flows \citep{Huggel2012}, %Caine1980,
or soil erosion and further adverse effects on terrestrial ecosystems \citep{Dotterweich2008,Knapp2008}. On a global scale, insured losses caused by floods -- not including storm surges -- dropped from~5.7 in~2016 (12\,\%) to~2.1~billion U.S.-dollars in~2017 (1.5\,\% of total insured losses), but still accounted for almost half of the victims of natural catastrophes \citep{Sigma2017,Sigma2018}. Central Europe experienced its last devastating flood event in May and June~2013, with Germany affected the most \citep{Merz2014}. With 16~billion U.S.-dollars in economic losses, it has been the costliest natural disaster in German history. The drivers of this event were large-scale intense and prolonged precipitation over the Elbe and Danube catchments with up to 400\,mm~of rain per day following a wet spring saturating the soils before the precipitation event. It is widely accepted that flood prevention measures reduced potential damage by a significant amount \citep{Wake2013}. In the context of such extreme events, the inference of building codes and design criteria for infrastructure, the long-term cautionary regulation of hydropower plants, or the pricing of insurance products requires information on the severity and frequency distributions of extreme weather events \citep{Messerli2014}. Statistical extreme value theory~EVT provides the necessary tools for estimating and extrapolating return values (intensity) and return periods (frequency) also beyond the range of observed values \citep[e.g.][]{McNeil2015}.

Global change adds a further level of complication to this task, as it puts the climate system in a non-stationary condition, with vital impacts on risk management. To make reliable predictions for the future, it is crucial to understand the dependencies of the hydrological cycle, and especially of extreme precipitation, on the general atmospheric state. Just as it is only considered \textit{likely} that overall precipitation increased in the Northern Hemisphere \citep[with \textit{high confidence} after 1951,][]{AR5Ch2}, there is also just "[...]  \textit{medium confidence} in trends in heavy precipitation in Europe, due to partly inconsistent signals across studies and regions, especially in summer" \citep[p.~142]{Seneviratne2012}. This statement emphasises the importance of assessing trends with a consistent methodology across regions and seasons. With increasing extreme precipitation intensity and frequency, the flooding risk in Central Europe is also expected to rise \citep{Hattermann2013}, potentially doubling the frequency of extreme flood losses \citep{Jongman2014}. The dependency of the European flood risk on climate change is to a large extent masked by the socio-economic development \citep{Barredo2009}, affecting both exposure and vulnerability. A more robust link can be established for the hazard component \citep{Lavell2012}, as physical laws govern the potential atmospheric water holding capacity as a function of its temperature, which is expected to heavily influence extreme precipitation events. The positive scaling between instantaneous ambient temperature and rainfall intensity is extensively studied (c.f.~Chapter~\ref{sec:physbas}), but only few studies assess the climatological sensitivity of extreme precipitation with regard to globally or regionally averaged temperature anomalies. Analyses were conducted on a global \citep{Westra2013}, continental \citep[for Australia,][]{Westra2011}, and a small-scale regional level \citep[for Switzerland,][]{Scherrer2016}, confirming the expected increase, but not yet for a larger European domain. As indicated by \citet{Seneviratne2012}, considerable regional and seasonal differences are found in European heavy precipitation trends, indicating substantial dynamical influence on local extreme precipitation scaling. Robust regional and seasonal estimates of the latter are valuable not only to the scientific and professional risk management community, but also for the public communication of changing extreme events \citep{Zanni2018}.


	\section{Research objectives}\label{ssec:intro_researchques}

In the context of the above motivation, the following objectives are addressed in this thesis:

\begin{mdframed}[backgroundcolor=gray!20]
\begin{itemize}  
\item \textbf{Perform a non-stationary extreme value analysis of Central European maximum precipitation data, to infer the scaling relation of extreme precipitation intensity with regional and global temperature anomalies.}
\item \textbf{Analyse the spatial variation of the scaling relationship and infer the dependence range or cluster size of the scaling coefficient.}
\item \textbf{Quantify significance and robustness of the results using bootstrapping analysis techniques.}
\end{itemize}
\end{mdframed}

 Added value compared to existing studies should be created by further analysing seasonal and multi-day maximum precipitation. So far, mostly just annual one-day extremes have been evaluated. Temporal trends in extreme precipitation intensity, frequency, and seasonal occurrence are also derived for comparability purposes. Although factors influencing heavy precipitation are assessed with regard to climate sensitivity to explain certain features found in the scaling estimates, this study does not aim to attribute changes in Central European extreme precipitation to human influence, as done on a global scale by \citet{Min2011}, or \citet{Pall2011} for the autumn~2000 flooding event in the United Kingdom. It should further be noted that explicitly no links with local temperature on the day of the event are analysed, though to this day most of the theory focusses on this type of extreme precipitation scaling. This has several reasons, first, because for example only~50 German records provide temperature data for more than~80~years since 1901. Second, precipitation data of higher than daily temporal resolution would be required as mostly high quantiles, but not maxima, are considered, which is only available from the late 20th~century onwards. And finally, it does not fit with the climatological perspective of finding a scaling between Central European extreme precipitation and the general thermodynamic state of the global or regional atmosphere. 

	\section{Thesis structure}\label{ssec:intro_thesstruct}

The thesis will be structured in the following way; Chapter~\ref{sec:physbas} examines the physical and meteorological basics of extreme precipitation scaling with temperature, separately looking at the thermodynamical (Section~\ref{ssec:physbas_thdyn}), dynamical (Section~\ref{ssec:physbas_dyn}), and microphysical component (Section~\ref{ssec:intro_microphys}). Furthermore, a somewhat simplified model for extreme precipitation is introduced in Section~\ref{ssec:physbas_oldmodel}, which is often used to motivate assumptions in the probabilistic treatment of precipitation. Finally, the current state of research regarding extreme precipitation scaling is presented in Section~\ref{ssec:physbas_CCscalingl}.

In Chapter~\ref{sec:data}, information on the underlying data is provided. Section~\ref{ssec:data_ind} first summarises the necessity and definition of indices, followed by Sections~\ref{ssec:data_precip} and~\ref{ssec:data_temp}, covering the sources of precipitation and temperature data, respectively. Finally, Section~\ref{ssec:data_inhomo} discusses potential issues related to observational data.

The methodological aspects are further examined in Chapter~\ref{sec:statbas}. General stationary precipitation statistics are the topic of Section~\ref{ssec:statbas_gen}, whereas Sections~\ref{ssec:statbas_temptrends} and~\ref{ssec:statbas_scaling} focus on the univariate trend and scaling estimation procedures. Section~\ref{ssec:statbas_spatial} further includes the spatial dimension, introducing the approaches chosen to deal with spatial correlation or determine field significance. As the discussion on scaling estimation techniques is rather extensive, minor derivation procedures which are assumed to be familiar to the reader are introduced directly when presenting the results, like linear trend regression in the occurrence dates, or the derivation of return periods and return values.

Chapter~\ref{sec:results} provides the results obtained from the statistical analysis. But first, the climatological context is outlined in Section~\ref{ssec:results_general}, followed by Sections~\ref{ssec:results_nonpar} and~\ref{ssec:results_evd}, where trends and scaling results are summarised. Section~\ref{ssec:deviation_cc_scaling} connects the latter with the content of Chapter~\ref{sec:physbas} by providing reasoning for deviations from purely thermodynamic Clausius-Clapeyron scaling. Implications on return periods and the potential of max-stable process models in this context are briefly discussed in Section~\ref{ssec:return_periods}.

Finally, conclusions are drawn in Chapter~\ref{sec:summary}, providing a synopsis of the core findings and an outlook on potentially interesting future research questions.


















\iffalse
%What does station data say on climate level, many studies mainly focussed on short-term (hourly) data with direct link. Is general (!) increase of extreme precip visible in data, and if so, is there spatial variation?
%(Importance  Sigma damages, link to floodings: \citep{Madsen2014})
%Basics of changing hydrological cycle (Trenberth1999, Allen2002, Muller2011)
%General precip changes with climate change: Global  IPCC, Contribution of human activity  Min-2011. Changes in mean precipitation in Central Europe \citep[chapter 9.6]{Lohmann2016}.
%See climate signal that was predicted first \citep{Fischer2016} but only observed now.
%Interesting for stakeholders \citep{Messerli2014}, application of new techniques transfers very rappidly \citep{Koch2016}

%	\section{State of current research}\label{ssec:intro_findings}
%Early papers: \citealp{Trenberth1999,Allen2002,Trenberth2003}

%Mismatch between observed and simulated precip trends \citep{Zhang2011}.

%a)	Short wrap-up on temperature scaling and spatial clustering of stations
%CC-Scaling findings
%Decline after 22�C / higher temperature in general (Utsumi-2011 daily, Chen-2016 sub-daily UK, Lenderink-2017)
%In models: thermodyn contribution below CC-scaling (OGorman2017), as projected in OGorman2009
%Super-CC-Scaling for hourly extremes (Many papers)
%Critic of using binning-method due to local cooling during extreme precip event (Bao-2017) -> important to use conditional mean temperature of the extreme precipitation event $T_e$, see section~\ref{ssec:physbas_thdyn}.

	\section{Research question}\label{ssec:intro_researchques}
Temporal trends in extreme precipitation intensity and frequency is analysed, but temperature scaling only for intensity (no direct physical link as CC-scaling for intensity).	

Detection, not attribution \citep{Zhang2013}, as done by \citep{Min2011,Zhang2013b} (latter for NH).
Not looking at relationship between (sub-daily) precipitation extremes and local temperatures observed simultaneously, since a) not available (daily resolution until 1900), b) variable, c) influenced by precipitation event, d) already done by many others (c.f.~Section~\ref{ssec:physbas_CCscaling}). For Germany, only 50~stations provide both daily precipitation and mean, minimum, and maximum temperature for more than 80~years. Also not using dew-point temperature, since not known, if then calculated using RH, RH would be the better covariate anyway. Using trend scaling in this thesis, not binning scaling \citep{Zhang2017}. "Need to reduce noise in the predictor by averaging over time or space of by pooling data from different locations when establishing precipitation and temperature relationships" \citep{Zhang2017}

If the system is understood well enough and can be modelled to a degree of detail comprising all necessary subprocesses leading to extreme events, models are a tool of studying changes in climate variables. Increases in heavy precipitation were first predicted in the 1980s based on rather coarse climate models \citep{Noda1989}, and also today models are still heavily used to further understand the physical processes of extreme precipitation (c.f.~Chapter~\ref{sec:physbas}) at a variety of different scales \citep{Loriaux2017,Pfahl2017}. Even though computational capabilities increased exponentially in the last decades, hardly any long-term coupled climate simulation including both global scale dynamics and thermodynamics as well as small-scale local cloud dynamics and microphysical processes is available to examine all aspects of extreme precipitation events. Even regional convection-permitting climate models are not yet widely available due to high computational costs \citep{Zhang2017}. Studies based on observational climate data face utterly different, but comparably limiting challenges. These are related to observational practices and data handling aspects, potentially leading to inhomogeneities in climate time series, as outlined in Chapter~\ref{sec:data}.%the following paragraphs. 


	\section{Thesis structure}\label{ssec:intro_thesstruct}
We operate with a level of significance $\alpha = 0.05$.
The steps for the core procedure, deriving scaling estimates from non-stationary extreme value theory (EVT, abk�rzung schon mal vorgekommen), are summarised in Chapter~\ref{sec:statbas}. Minor analyses are sometimes only introduced when presenting the results in Section~\ref{sec:results}



	
Alone in 2016 the 65 most devastating flood events globally accounted for roughly 3300 victims and over five billion dollars in insured damage \citep{Sigma2017}. Few of these floods are caused by storm surges, but most are directly related to extensive precipitation events, whereof especially flash floods are due to short duration precipitation events of extreme intensity. In the context of global climate change, precipitation patterns are likely to be affected, and therefore the evolution of extreme precipitation should be examined more closely. The following section will provide background information on the current knowledge about changes in extreme precipitation in general, with a focus on observational studies. Briefly, the implications of a changing climate on extreme value based studies will be addressed, as well as the way operational meteorological services are taking the issue of non-stationarity into account when communicating risk to their user community. 

%Additionally, a short overview on the implications of a changing climate on studies based on extreme value theory will be provided, and on how operational meteorological services have responded to this complication. 
% Europe: Spain 26.11.?5.12. Floods, rainstorms 2 dead/1 injured/EUR 60mn (USD 63mn) insured loss

	\section{Observed Changes in Extreme Precipitation}\label{ssec:bg_obs}

Based on extreme precipitation indices (see Section~\ref{ssec:methdata_def}), several studies already explored measurement and model output data, both globally and on regional levels, in order to detect possible changes in extreme precipitation. Global studies found that in a majority of regions, observed rare precipitation events are getting both more intense and frequent over time, but trends show a rather heterogeneous spatial pattern \citep{Groisman2005,Donat2013a}. Such global studies are generally restricted to the use of preprocessed, openly available datasets (GHCNDEX or HadEX2, please refer to section~\ref{ssec:methdata_datasets} for further information) and apply rather general regression techniques to infer local change estimates. Examples for more regional assessments are \citet{Zolina2009}, who investigate changes of quantiles derived from fitting probability distributions to the data, or \citet{Scherrer2016}, focussing on the scaling of intensity and frequency with surface temperature. The basis for such detailed analyses is fine-resolution observation data provided by local weather services, restricting the scope of the findings to specific regions. In summary, the Intergovernmental Panel on Climate Change (IPCC) Assessment Report Five therefore concludes that "[...] it is \textit{likely} that since 1951 there have been statistically significant increases in the number of heavy precipitation events [...], but there are strong regional and sub-regional variations in the trends." \citep[p.~213]{AR5Ch2}.


	\section{Modelled Changes in Extreme Precipitation}\label{ssec:bg_proj}

Since model results are not the focus of the thesis, this subsection will just briefly summarise the main modelling results of extreme precipitation change studies. In general, discrepancies between models and reanalyses concerning heavy precipitation are strongly dependent on model resolution, and models are found to often underestimate the magnitude of historical trends \citep{AR5Ch9}. Even though, according to IPCC Assessment Report Five, there is just "[...] \textit{medium confidence} that anthropogenic forcing has contributed to a global scale intensification of heavy precipitation over the second half of the 20th century in land regions where observational coverage is sufficient for assessment" \citep[p.~912]{AR5Ch10}, extreme precipitation events will still "[...] \textit{very likely} be more intense and more frequent in these regions in a warmer climate." \citep[pp.~1086]{AR5Ch12}. In the context of Switzerland, there is currently \textit{medium} confidence in a "[...] weak tendency towards more intense rainfall events in autumn, [and a] potential increase in summer and winter", where "major changes cannot be ruled out" \citep[p.~49]{CH2011}.


	\section{Implications for Extreme Value Theory Studies}\label{ssec:bg_ext}

Instead of focussing on changes in high quantiles, extreme value theory concentrates on the change in maximum precipitation. Whereas for the former also more moderate extremes are examined (investigating 90\% or 95\% quantiles), the latter, in contrast, requires sufficient data to examine trends in (by definition) extremely rare precipitation events. Having vast global or high-resolution local datasets at hand, such inference becomes quite possible. Additionally, climate change violates the standard assumption of stationarity, which has serious consequences on the application of extreme value theory \citep{Katz2010}. The hypothesis of increasing extreme precipitation is supported both by findings mentioned in subsection~\ref{ssec:bg_obs} and also in several recent studies based on extreme value statistics. \citet{Besselaar2013} investigated changes in five, ten and twenty year return periods for both northern and southern Europe of one and five day extreme precipitation events. Across all seasons, return periods decreased by up to 58\%. \citet{Westra2013} fitted an extreme value distribution to yearly one day precipitation maxima of the HadEX2 dataset, with a parameter depending on global average near-surface temperature. Solid evidence for a distinct relation of extreme daily precipitation and surface temperature were established, with a median increase of 7.7\%~K$^{-1}$, but with considerable meridional variation. This scaling of extreme precipitation with temperature is consistent with the hypothesis of \citet{Trenberth2003}, who connect it to the Clausius--Clapeyron relation, which explains the non-linear increasing water holding capacity of air with increasing temperature. 
%These findings are consistent with the overall increasing number and intensity of extreme precipitation events, as discussed by \citet{Trenberth2003}.


	\section{Return Value and Return Period Products}\label{ssec:bg_metoff}
	
Providing specific return values or periods is a delicate matter, especially since estimates of return periods and values outside the range of observation are subject to big uncertainty. According to C. Appenzeller (personal communication, 27 April 2017), most European met services provide extreme value analyses only on request. The European Climate Assessment \& Dataset (ECA\&D) webpage also provides return value estimates, but for past 20-year base periods only (\url{http://eca.knmi.nl/returnvalues/index.php}). The United States National Atmospheric and Oceanographic Administration (NOAA) produced frequency estimates of heavy precipitation events from 1960s onwards, already applying early advances in extreme value theory \citep{Hershfield1963}. Furthermore, NOAA and the German Weather Service (DWD) provide estimates of probable maximum precipitation, which is the maximum possible precipitation under the assumption of stationary climate conditions, but whereas DWD analyses are not available free-of-charge, NOAA did not continue theirs due to ceased funding. % COMMENT: Is this the all time maximum? (if so the underlying methodology is questionable)
The Swiss national weather and climate service (MeteoSwiss) is one of the few met services providing return period and return level analyses at station level (\url{http://www.meteoswiss.admin.ch/home/climate/past/climate-extremes/extreme-value-analyses/standard-period.html}). Estimates by MeteoSwiss are made under the assumption of stationarity \citep{Fukutome2015}, which is likely to be violated also in Switzerland \citep{Scherrer2016}. This has consequences not only on the estimation procedure, but also on how risk of extreme precipitation events should be communicated. It is no longer appropriate to refer to risk in terms of fix return periods or levels under non-stationarity, since these are now variable as well. \citet{Messerli2014} assessed the needs of the concerned communities, stating that both the influence of climate change on extreme precipitation is of interest, and also that areal analyses should be considered. Also the Swiss Federal Office for the Environment (FOEN) is planning to include non-stationarity in their extreme runoff analyses \citep{Baumgartner2013}.

\fi
















